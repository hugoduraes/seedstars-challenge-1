'use strict';

(function () {
  var hnWrapper = require('./hn-wrapper');
  var debounce = require('lodash.debounce');

  var hnHandler = (function () {
    var _el;
    var _ul;
    var _limit;
    var _offset = 0;

    /**
     * Handle a single story loaded from HN API
     *
     * @param  {Object}  story  Story loaded from HN API
     */
    var handleStory = function (story) {
      var a = document.createElement('a');
      var li = document.createElement('li');
      li.setAttribute('class', 'story-item');

      a.setAttribute('href', story.url);
      a.setAttribute('target', '_blank');
      a.innerHTML = story.title;

      li.appendChild(a);

      _ul.appendChild(li);
    };

    /**
     * Handles stories loaded from HN API
     *
     * @param  {Array}    stories Stories loaded from HN API
     * @return {Promise}
     */
    var handleStories = function (stories) {
      var previousPromise;

      // advance offset
      _offset = _offset + _limit;

      previousPromise = Promise.resolve();

      stories.forEach(function (storyId) {
        previousPromise = previousPromise
          .then(function () {
            return hnWrapper.getStory(storyId);
          })
          .then(handleStory)
        ;
      });

      return previousPromise;
    };

    return {
      /**
       * Initialize handler
       * @param  {Element}  el    DOM Element on which stories should be loaded
       */
      init: function (el) {
        _el = el;
        _ul = document.createElement('ul');
        _ul.setAttribute('class', 'stories-list');

        _el.appendChild(_ul);
      },

      /**
       * Fetches stories from HN API and loads them into the DOM
       *
       * @param  {Number}   limit Number of stories to load at each iteration
       */
      loadStories: function (limit) {
        _limit = limit || 10;

        return hnWrapper
          .getTopStories(_limit, _offset)
          .then(handleStories)
        ;
      }
    };
  })();

  /**
   * Checks if user has scrolled to bottom
   *
   * @return {Boolean}
   */
  var hasScrolledToBottom = function () {
    var scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
    var scrollHeight = (document.documentElement && document.documentElement.scrollHeight) || document.body.scrollHeight;

    return (scrollTop + window.innerHeight) >= scrollHeight;
  };

  /**
   * Checks if document has vertical scrollbars
   *
   * @return {Boolean}
   */
  var hasScroll = function () {
    var scrollHeight = (document.documentElement && document.documentElement.scrollHeight) || document.body.scrollHeight;

    return window.innerHeight < scrollHeight;
  };

  /**
   * Loads stories while there's no scroll on page
   */
  var loadStories = function (limit) {
    hnHandler
      .loadStories(limit)
      .then(function () {
        if (!hasScroll()) {
          loadStories(1);
        }
      })
    ;
  };

  var el = document.getElementById('top-stories');
  var limit = 10;

  // start
  hnHandler.init(el);
  loadStories(limit);

  // scroll event handler
  document.addEventListener('scroll', debounce(function (event) {
    if (hasScrolledToBottom()) {
      hnHandler.loadStories(limit);
    }
  }, 250));
})();
