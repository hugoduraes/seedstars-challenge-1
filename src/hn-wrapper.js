'use strict';

var request = require('superagent');

module.exports = (function () {
  var cache = {
    topStories: [],
    items: {}
  };

  var endpoints = {
    topstories: 'https://hacker-news.firebaseio.com/v0/topstories.json',
    item: 'https://hacker-news.firebaseio.com/v0/item/__ID__.json'
  };

  var getItemsIds = function () {
    var endpoint = endpoints.topstories;

    return new Promise(function (resolve, reject) {
      if (cache.topStories.length > 0) {
        return resolve(cache.topStories);
      }

      request
        .get(endpoint)
        .end(function (err, res) {
          if (err || !res.ok) {
            return reject('error fetching endpoint "' + endpoint + '"');
          }

          cache.topStories = res.body;

          return resolve(cache.topStories);
        })
      ;
    });
  };

  var getItem = function (id) {
    var endpoint = endpoints.item.replace('__ID__', id);

    return new Promise(function (resolve, reject) {
      if (cache.items[id] !== undefined) {
        return resolve(cache.items[id]);
      }

      request
        .get(endpoint)
        .end(function (err, res) {
          if (err || !res.ok) {
            return reject('error fetching endpoint "' + endpoint + '"');
          }

          cache.items[id] = res.body;

          return resolve(cache.items[id]);
        })
      ;
    });
  };

  return {
    getTopStories: function (limit, offset) {
      var _limit = limit || 10;
      var _offset = offset || 0;

      return getItemsIds()
        .then(function (items) {
          return items.slice(_offset, _limit + _offset);
        })
      ;
    },

    getStory: function (id) {
      return getItem(id);
    }
  };
})();
