# Seedstars Challenge #1

HackerNews infinite scroll news listing page.

## Assumptions

**Ajax Requests**

  For ajax requests to the HackerNews API I've used a third-party library: [SuperAgent](http://visionmedia.github.io/superagent/).

## Usage

```
# clone repository
git clone https://hugoduraes@bitbucket.org/hugoduraes/seedstars-challenge-1.git

# change directory
cd seedstars-challenge-1

# install dependencies
npm install

# start app
npm start
```

*Visit [http://localhost:8080]() to view the page.*
